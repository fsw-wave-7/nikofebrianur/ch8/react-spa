import React, { Component, Fragment } from 'react'
import Chapter from './chapter'

class Subtitle extends Component {

    constructor(props) {
        super(props)
        this.state = {
            title: 'Learn ReactJS',
            currentChapter: '7'
        }
        this.changeTitle = this.changeTitle.bind(this)
    }

        changeTitle() {
            this.setState({
                title: 'Belajar ReactJs'
            })
    }

    render() {
        const { username, chapter } = this.props
        const { title, currentChapter } = this.state

        return (
            <Fragment>
                <p> {title}
                Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                  className="App-link"
                  href="https://reactjs.org"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                    Hello, {username}
                    Belajar React di chapter        {chapter} cuy! Wow!!
                </a>
                <Chapter chapter= {currentChapter}/>
                <button onClick={this.changeTitle}>Ubah Judul</button>
        
            </Fragment>
        )   
    }
}

export default Subtitle