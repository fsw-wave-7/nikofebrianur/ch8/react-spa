import React, { Component } from 'react'
import styles from '../css/chapter.module.css'

class Chapter extends Component {
    render() {
        const { chapter } = this.props
        
        return <p className={StyleSheet.chapter + " " + styles.large }>Chapter { chapter }</p>
    }
}

export default Chapter